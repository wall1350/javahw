import java.util.Set;
import java.util.Vector;

public class GateNOT extends Device{
    boolean result = false;
    protected Vector<Device> iPins;
    protected Vector<Device> nextDevices;
    public GateNOT() {
        iPins = new Vector<>();
        nextDevices = new Vector<>();
    }
    public void clearInput() {
        iPins = new Vector<>();
        nextDevices = new Vector<>();
    }
    public void addInputPin(Device iPin)
    {
        // complete this method by yourself
        result = !iPin.getOutput();
    }
    public void setInput(boolean value)
    {
        throw new  RuntimeException("GateNOT don't support method");
    }

    public boolean getOutput()
    {
        // complete this method by yourself
        return result;
    }

}
