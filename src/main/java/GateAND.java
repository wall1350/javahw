import java.util.Set;
import java.util.Vector;

public class GateAND extends Device {
    boolean result = false;
    protected Vector <Device> iPins;
    protected Vector<Device> nextDevices;
    public GateAND() {
        iPins = new Vector <> ();
        nextDevices = new Vector<>();
    }
    public void clearInput() {
        iPins = new Vector <> ();
        nextDevices = new Vector<>();
    }
    public void addInputPin(Device iPin) {
        // complete this method by yourself
        iPins.add(iPin);
    }
    public void setInput(boolean value) {
        throw new RuntimeException("GateAND don't support method");
    }

    public boolean getOutput() {
        // complete this method by yourself

        for (int i = 0; i < iPins.size(); i++) {
            if (i == 0) {
                result = iPins.get(0).getOutput();
            } else {
                result = result & iPins.get(i).getOutput();
            }
        }
        return result;
    }



}