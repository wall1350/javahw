import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import static java.lang.Math.pow;

public class LogicSimulator
{
    private Vector<Device> circuits;//存放gate本身
    private Vector<Device> iPins;   //每個input的值
    private Vector<Device> oPins;   //最終解答
    private Vector<String> commands;//紀錄電路怎麼連線用

    private int howManyGate = 0;    //有幾個gate
    private int howManyInput = 0;   //有多少輸入
    private int gateIndex = 0;      //目前輪到第幾個gate了

    private int whoIsFinal[] ;
    private int howManyOutPut = 0;

    public LogicSimulator(){
        circuits = new Vector<Device>();
        iPins = new Vector<Device>();
        oPins = new Vector<Device>();
        commands = new Vector<String>();

    }

    public void load(String file1Path) throws IOException {
        FileReader fr = new FileReader(file1Path);
        BufferedReader br = new BufferedReader(fr);
        int lineNUM = 0;//文件當中有幾行
        while (br.ready()) {
            lineNUM++;
            String str = br.readLine();

            //查詢有幾個input pin
            if(lineNUM==1){
                howManyInput=Integer.valueOf(str);
                for (int i=0;i<howManyInput;i++){
                    this.iPins.add(new IPin());
                }
            }

            // 查詢有幾個 gate
            else if(lineNUM==2){
                howManyGate=Integer.valueOf(str);
                whoIsFinal = new int[howManyGate];
                for (int i=0;i<howManyGate;i++){
                    this.circuits.add(new Device());
                    this.oPins.add(new Device());
                    whoIsFinal[i]=1;
                }
            }

            //構建每個 gate
            else {
                commands.add(str);
                String[] tokens = str.split(" ");
                //讀取並判斷這是哪種gate  eg: 1 -1 2.1 3.1 0 第一個數字代表and gate
                int gateKind = Integer.valueOf(tokens[0]);

                if (gateKind == 1){
                    this.circuits.set(gateIndex,new GateAND());
                }else if  (gateKind == 2){
                    this.circuits.set(gateIndex,new GateOR());
                }else if (gateKind==3){
                    this.circuits.set(gateIndex,new GateNOT());
                }

                for (int i=1;i<tokens.length;i++){
                    float op = Float.valueOf(tokens[i]);
                    if (op>0){
                        int checkFinalIndex =  (int)op -1;
                        whoIsFinal[checkFinalIndex]=0;
                    }
                }
                gateIndex++;// 換下一個gate
            }// End of else

        }//End of while
        fr.close();

        for (int i=0;i<howManyGate;i++){
            if (whoIsFinal[i]!=0){ //如果都沒有被別人所用，及為終點
                howManyOutPut++;
            }
        }

    }//end of method

    public int getHowManyOutPut(){
        return howManyOutPut;
    }

    public String getSimulationResult(Vector<Boolean> inputValues)  {
        String i1 = inputValues.get(0) ? "1" : "0";
        String i2 = inputValues.get(1) ? "1" : "0";
        String i3 = inputValues.get(2) ? "1" : "0";

        for (int i=0;i<howManyInput;i++){ //透過讀取 inputValues ，將每一個布林值放入到iPin上
            IPin iPin = new IPin();
            iPin.setInput(inputValues.get(i));
            iPins.set(i,iPin);
        }
        dealWithCircuits();

        String  result="";
        String str = "";
        for (int i =0;i<circuits.size();i++){
            if(whoIsFinal[i]!=0){ //如果該元件輸出沒有被其他元件所用，視為終點

                oPins.set(i,circuits.get(i));

                result = result + (oPins.get(i).getOutput() ? " 1" : " 0");
            }
        }

        str += i1+" "+i2+" "+i3+" |"+result+"\n";

        return str;
    }

    private void dealWithCircuits() {
        // 讀取該如何接線
        for (int commandIndex =0;commandIndex<circuits.size();commandIndex++){
            circuits.get(commandIndex).clearInput();
            String[] tokens = commands.get(commandIndex).split(" ");

            for (int i=1; i<tokens.length;i++ ){
                float op = Float.valueOf(tokens[i]);
                if (op<0){ //如果是負數就是讀取pin
                    int pinInputIndex = -1 * (int) op;
                    pinInputIndex = pinInputIndex -1;

                    circuits.get(commandIndex).addInputPin(this.iPins.get(pinInputIndex));
                }else if (op>0){//不然的話就是讀取其他gate的output
                    int outputIndex =  (int) op;
                    outputIndex = outputIndex -1;


                    circuits.get(commandIndex).addInputPin(this.circuits.get(outputIndex));
                }
                else{}
            }

        }//end of outside for

    }

    public String getTruthTable() {

        String binaryStr ="";
        String[] bits;
        Vector<Boolean> inputValues = new Vector<Boolean>();
        String str = "";

        //產生true table
        for (int j=0;j<pow(2, howManyInput);j++){
            binaryStr = binaryTransform(j);
            bits = binaryStr.split(""); // 111 -> [1,1,1]
            inputValues.clear();

            for (int i=0;i<howManyInput;i++){ //透過讀取 inputValues ，將每一個布林值放入到iPin上
                boolean b = bits[i].equals("1")? true:false;
                inputValues.add(b);
            }

            str = str + getSimulationResult(inputValues);
        }

            return str;
    }

    public  String produceAboveTitle() {

        String []aboveTitle = new String[4];
        for (int i=0;i<aboveTitle.length;i++){
            aboveTitle[i]="";
        }
        aboveTitle[0] = "Simulation Result:\n";
        for (int i=0;i<=howManyInput;i++){
            if (i!= howManyInput){
                aboveTitle[1]+="i ";
                aboveTitle[2]+= Integer.toString(i+1)+" ";
                aboveTitle[3]+= "--";
            }else{
                aboveTitle[1]+="|";
                aboveTitle[2]+="|";
                aboveTitle[3]+= "+";
            }
        }

        for (int i=0;i<=howManyOutPut;i++){
            if (i!= howManyOutPut){
                aboveTitle[1]+=" o";
                aboveTitle[2]+= " "+Integer.toString(i+1);
                aboveTitle[3]+= "--";
            }else{
                aboveTitle[1]+="\n";
                aboveTitle[2]+="\n";
                aboveTitle[3]+="\n";
            }
        }

        String str = "";
        for (int i =0;i<aboveTitle.length;i++){
            str += aboveTitle[i];
        }
        return str;
    }


    public String binaryTransform(int i ){      //二進位轉換機制，3個bits版本
        String ans  = Integer.toBinaryString(i);
        if (ans.length()==1){
            ans = "00"+ans;
        }
        if(ans.length()==2){
            ans = "0"+ans;
        }
        return  ans;
    }

}