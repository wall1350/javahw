import java.util.Set;
import java.util.Vector;

public class Device
{
    protected Vector<Device> iPins;
    protected Vector<Device> nextDevices;

    public Device()
    {
        iPins = new Vector<>();
        nextDevices = new Vector<>();
    }

    public void clearInput() {
        iPins = new Vector<>();
        nextDevices = new Vector<>();
    }

    public void addInputPin(Device iPin)
    {
        // complete this method by yourself
        iPins.add(iPin);
    }

    public void setInput(boolean value)
    {
        throw new  RuntimeException("You should implement this method");
    }

    public boolean getOutput()
    {
        // complete this method by yourself
        throw new  RuntimeException("You should implement this method");
    }

}