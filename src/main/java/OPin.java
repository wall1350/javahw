public class OPin extends Device
{
    boolean result = false;
    @Override
    public void setInput(boolean value)
    {
        this.result = value;
    }

    @Override
    public boolean getOutput()
    {
        return this.result;
    }

}