import java.util.Set;
import java.util.Vector;

public class IPin extends Device
{
    boolean result = false;
    protected Vector<Device> nextDevices = new Vector<>();

    public IPin(){
        nextDevices = new Vector<>();
    }

    @Override
    public void setInput(boolean value)
    {
        this.result = value;
    }

    @Override
    public boolean getOutput()
    {
        return this.result;
    }


}