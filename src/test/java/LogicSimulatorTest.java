import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LogicSimulatorTest {
    String file1Path;
    String file2Path;

    @Before
    public void setUp()
    {
        file1Path = "src/File1.lcf";
        file2Path = "src/File2.lcf";
    }
    @Test
    public void testGetSimulationResult() throws IOException {
        LogicSimulator logicSimulator = new LogicSimulator();
        logicSimulator.load(file1Path);

        //011
        Vector<Boolean> inputValues = new Vector<>();
        inputValues.add(false);
        inputValues.add(true);
        inputValues.add(true);

        assertEquals("Simulation Result:\n" +
                "i i i | o\n" +
                "1 2 3 | 1\n" +
                "------+--\n" +
                "0 1 1 | 0\n", logicSimulator.produceAboveTitle()+logicSimulator.getSimulationResult(inputValues));
    }

    @Test
    public void binaryTransform()throws IOException{
        int i = 7;
        String ans  = Integer.toBinaryString(i);
        if (ans.length()==1){
            ans = "00"+ans;
        }
        if(ans.length()==2){
            ans = "0"+ans;
        }

//        String[] tokens = ans.split("");
//        for (String token : tokens){
//            System.out.println(token);
//        }

        assertEquals("111", ans);
    }

    @Test
    public void testGetTruthTable() throws IOException {
        LogicSimulator logicSimulator = new LogicSimulator();
        logicSimulator.load(file2Path);
        assertEquals("Simulation Result:\n" +
                "i i i | o o\n" +
                "1 2 3 | 1 2\n" +
                "------+----\n" +
                "0 0 0 | 0 1\n" +
                "0 0 1 | 0 1\n" +
                "0 1 0 | 0 1\n" +
                "0 1 1 | 0 1\n" +
                "1 0 0 | 1 0\n" +
                "1 0 1 | 1 0\n" +
                "1 1 0 | 0 1\n" +
                "1 1 1 | 0 1\n", logicSimulator.produceAboveTitle()+logicSimulator.getTruthTable());
    }

    @Test
    public void testTitle(){
        int howManyInput=3;
        int howManyOutPut=1;
        String []aboveTitle = new String[4];
        for (int i=0;i<aboveTitle.length;i++){
            aboveTitle[i]="";
        }
        aboveTitle[0] = "Simulation Result:\n";
        for (int i=0;i<=howManyInput;i++){
            if (i!= howManyInput){
                aboveTitle[1]+="i ";
                aboveTitle[2]+= Integer.toString(i+1)+" ";
                aboveTitle[3]+= "--";
            }else{
                aboveTitle[1]+="|";
                aboveTitle[2]+="|";
                aboveTitle[3]+= "+";
            }
        }

        for (int i=0;i<=howManyOutPut;i++){
            if (i!= howManyOutPut){
                aboveTitle[1]+=" o";
                aboveTitle[2]+= " "+Integer.toString(i+1);
                aboveTitle[3]+= "--";
            }else{
                aboveTitle[1]+="\n";
                aboveTitle[2]+="\n";
                aboveTitle[3]+="\n";
            }
        }

        String str = "";
        for (int i =0;i<aboveTitle.length;i++){
            str += aboveTitle[i];
        }

        System.out.println(str);
        assertEquals("Simulation Result:\n" +
                "i i i | o\n" +
                "1 2 3 | 1\n" +
                "------+--\n",str);
    }

}
